# React Boilerplate

## Getting Started

### 1. Setup hooks (Linux)

First, check Git version `git --version` then

On Git `version >= 2.9`

```/bin/bash
git config core.hooksPath .githooks
```

On Git `version < 2.9` (Normal symlink)

```/bin/bash
find .git/hooks -type l -exec rm {} \; && find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;
```

### 2. Run project

Install dependencies

```/bin/sh
yarn
```

On development, run

```/bin/sh
yarn dev
```
