import shell from 'shelljs';

export async function install(packageDir: string): Promise<void> {
    const currentDir = process.cwd();

    await new Promise((resolve, reject) => {
        shell.cd(packageDir);

        const cmd = 'yarn';

        console.log(`\n> ${cmd}`);

        shell.exec(cmd, (code, stdout, stderr) => {
            shell.cd(currentDir);

            if (code !== 0) {
                reject(stderr);
            }
            else {
                resolve();
            }
        });
    });
}