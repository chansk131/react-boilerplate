import { join } from 'path';
import { existsSync, readFileSync, writeFileSync } from 'fs';
import chalk from 'chalk';
import rimraf from 'rimraf';

export function applyTemplate(appName: string): void {
    const dir = join(process.cwd(), appName);

    console.log('\n> Applying templates...');

    const filePath = `${dir}/package.json`;
    if (!existsSync(filePath)) {
        console.log(`${chalk.yellow("NOT FOUND")} ${filePath}`);
    }

    let template = readFileSync(filePath, "utf8");
    template = template.replace(new RegExp(`react-template`, "g"), appName);

    writeFileSync(filePath, template);
}

export function removeUnusedProperties(appName: string): void {
    const dir = join(process.cwd(), `${appName}`);

    const filePath = readFileSync(`${dir}/package.json`, 'utf8');
    const json = JSON.parse(filePath);

    delete json.templateFiles;

    writeFileSync(`${dir}/package.json`, JSON.stringify(json, null, '\t'));
}

export function deleteFolder(appName: string): void {
    try {
        rimraf.sync(join(process.cwd(), appName));
    }
    catch (err) {
        console.log(`Cannot delete the "${appName}" folder.`, err);
        throw err;
    }
}