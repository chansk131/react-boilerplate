import chalk from 'chalk';
const { exec } = require("child_process");

export async function clone(url: string, dir: string = '') {
    await new Promise((resolve, reject) => {
        const cmd = `git clone ${url} ${dir}`;

        console.log(`\n> ${cmd}`);

        exec(cmd, (error: Error, stdout:string, stderr: string) => {
            if (error) {
                reject(stderr);
            }
            else {
                console.log(`${chalk.green('CLONED')} ${url}`);
                resolve();
            }
        });
    });
}

