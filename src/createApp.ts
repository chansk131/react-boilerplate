/** @format */
import chalk from "chalk";
import inquirer, { QuestionCollection } from "inquirer";

import { clone } from "./git.service";
import { applyTemplate, deleteFolder } from "./file.service";
import { install } from "./npm.service";

type RepositoryKey = "🚀 Backoffice Template" | "👍 Blank Template";
const repositories: Record<RepositoryKey, string> = {
  "🚀 Backoffice Template":
    "https://gitlab.com/chansk131/react-backoffice-template.git",
  "👍 Blank Template":
    "-b blank-template https://gitlab.com/chansk131/react-backoffice-template.git",
};

export async function init(appName: string): Promise<void> {
  const questions: QuestionCollection<{ repository: RepositoryKey }> = {
    name: "repository",
    message: "Select template",
    type: "list",
    choices: Object.keys(repositories),
  };
  const answer = await inquirer.prompt(questions);
  const gitRepo = repositories[answer.repository];

  await clone(gitRepo, appName);
  applyTemplate(appName);
  deleteFolder(`${appName}/.git`);
  await install(appName);
  console.log(chalk.green(`\ncd ${appName} \nyarn start`));
}
