#!/usr/bin/env node
"use strict";

import chalk from 'chalk';
import inquirer, { QuestionCollection } from 'inquirer';

import { init } from "./createApp";

let [, , appName] = process.argv;

const questions: QuestionCollection<{ appName: string }> = {
	name: "appName",
	message: "Enter app name",
	type: "input"
};
(async () => {
	if (!appName) {
		const correctAppName = /^[\w\-.]+$/;

		for (; ;) {
			appName = (await inquirer.prompt(questions)).appName;
			if (correctAppName.test(appName)) {
				break;
			}
			console.log(chalk.red('appName is invalid format, please re-enter (without white space or special characters)\neg. my-app'));
		}
	}
	init(appName);
})();