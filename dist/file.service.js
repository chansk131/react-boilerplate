"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteFolder = exports.removeUnusedProperties = exports.applyTemplate = void 0;
var path_1 = require("path");
var fs_1 = require("fs");
var chalk_1 = __importDefault(require("chalk"));
var rimraf_1 = __importDefault(require("rimraf"));
function applyTemplate(appName) {
    var dir = path_1.join(process.cwd(), appName);
    console.log('\n> Applying templates...');
    var filePath = dir + "/package.json";
    if (!fs_1.existsSync(filePath)) {
        console.log(chalk_1.default.yellow("NOT FOUND") + " " + filePath);
    }
    var template = fs_1.readFileSync(filePath, "utf8");
    template = template.replace(new RegExp("react-template", "g"), appName);
    fs_1.writeFileSync(filePath, template);
}
exports.applyTemplate = applyTemplate;
function removeUnusedProperties(appName) {
    var dir = path_1.join(process.cwd(), "" + appName);
    var filePath = fs_1.readFileSync(dir + "/package.json", 'utf8');
    var json = JSON.parse(filePath);
    delete json.templateFiles;
    fs_1.writeFileSync(dir + "/package.json", JSON.stringify(json, null, '\t'));
}
exports.removeUnusedProperties = removeUnusedProperties;
function deleteFolder(appName) {
    try {
        rimraf_1.default.sync(path_1.join(process.cwd(), appName));
    }
    catch (err) {
        console.log("Cannot delete the \"" + appName + "\" folder.", err);
        throw err;
    }
}
exports.deleteFolder = deleteFolder;
